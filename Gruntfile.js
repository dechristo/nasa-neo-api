/**
 * Created by Luiz Eduardo de Christo
 * December 3th, 2017
 * Gruntfile.js
 */

module.exports = function(grunt) {

    grunt.initConfig({

        notify_hooks: {
            options: {
                enabled: true,
                title: 'Near Earth Objects API',
                duration: 5
            }
        },

        jshint: {
            files: ['Gruntfile.js', 'server.js', 'src/router.js','tests/**/*.spec.js', 'src/**/*.js']
        },

        watch: {
            files: ['<%= jshint.files %>'],
            tasks: ['jshint']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-notify');

    grunt.task.run('notify_hooks');
    grunt.registerTask('default', ['jshint', 'watch']);
};