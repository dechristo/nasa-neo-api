var NeoService = require('../services/neoService');
var DateUtils = require('../utils/dateUtils');
var dtUtils = new DateUtils();
var neoService = new NeoService();

exports.getHazardous = function(req, res) {
    neoService.getHazardous(req,res, function (result) {
        res.status(200).json(result);
    });
};

exports.getFastest = function(req, res) {
    neoService.getFastest(req,res, function (result) {
        res.status(200).json(result);
    });
};

exports.getBestYear = function(req, res) {
    neoService.getBestYear(req, res, function(result) {
        res.status(200).json(result);
    });
};

exports.getBestMonth = function(req, res) {
    neoService.getBestMonth(req, res, function(result) {
        var month = dtUtils.getFullMonthName(result);
        res.status(200).json(month);
    });
};
