var dateFormat = require('dateformat');

var dateUtils = function() {
    self = this;
    self.defaultFormat = 'yyyy-mm-dd';
};

dateUtils.prototype.subtractDays = function(date, quantity){

    if(!date) {
        var today = new Date();
        date = today;
    }

    var daysAgo = new Date().setDate(date.getDate() - quantity);
    var formattedDaysAgo = dateFormat(daysAgo, self.defaultFormat);

    return formattedDaysAgo;
};

dateUtils.prototype.getTodayOnDefaultFormat = function() {
  return dateFormat(new Date(), self.defaultFormat);
};

dateUtils.prototype.getFullMonthName = function(abbreviation){
    switch(abbreviation) {
        case 'Jan' : return 'January';
        case 'Feb' : return 'February';
        case 'Mar' : return 'March';
        case 'Apr' : return 'April';
        case 'May' : return 'May';
        case 'Jun' : return 'June';
        case 'Jul' : return 'July';
        case 'Aug' : return 'August';
        case 'Sep' : return 'September';
        case 'Oct' : return 'October';
        case 'Nov' : return 'November';
        case 'Dec' : return 'December';
    }
};
module.exports = dateUtils;