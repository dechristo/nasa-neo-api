var request = require('request');
var dateUtils = ('../utils/dateUtils');
var NEO = require('../models/nearEarthObject');
var self;

var neoService = function() {
    self = this;
};

neoService.prototype.neos = [];

neoService.prototype.extract = function(start, end, callback) {
    var url = self.getUrl(start,end);

    request(url, function (err, res) {
        if (err) return next('error:' + err.error);
        self.neos = JSON.parse(res.body).near_earth_objects;
        var arrNeos = [];

        for(var date in self.neos) {
            for(var item in self.neos[date]) {
                var newNeo = new NEO({
                    date : date,
                    reference : self.neos[date][item].neo_reference_id,
                    name : self.neos[date][item].name,
                    speed : self.neos[date][item].close_approach_data[0].relative_velocity.kilometers_per_hour,
                    isHazardous : self.neos[date][item].is_potentially_hazardous_asteroid
                });
                arrNeos.push(newNeo);
                self.save(newNeo, self.printError);
            }
        }
        callback(self.neos);
    });
};

neoService.prototype.extractLast3Days = function(callback) {
    var today = dateUtils.getTodayOnDefaultFormat();
    var threeDaysAgo = dateUtils.subtractDays(null,3);

    this.extract(today, threeDaysAgo, function() {
        callback(self.neos);
    });
};

neoService.prototype.getHazardous = function(req, res, next) {
    NEO.find({isHazardous : true}, function(err, docs) {
        if (!err){
            next(docs);
        } else {
            console.log('Error:' + err);
            next(err);
        }
    });
};

neoService.prototype.getFastest = function(req, res, next) {
    var speeds = [];
    var hazardous = req.params.hazardous ? req.params.hazardous : false;

    NEO.find({isHazardous : hazardous}, function(err, result) {
        if (!err){
            for (var neoObj in result) {
                speeds.push(result[neoObj]._doc.speed);
            }
            var fastest = speeds.reduce(function(previous, current) {
                return Math.max(previous, current);
            });
            next(fastest);
        }else {
            console.log('Error:' + err);
        }
    });
};

neoService.prototype.getBestYear = function(req, res, next) {
    var hazardous = req.params.hazardous ? req.params.hazardous : false;

    NEO.find({isHazardous : hazardous}, function(err, result) {
        if (!err){
            var bestYear = self.extractDatePart(result, 3); //index 3 corresponds to month on date string
            next(bestYear);
        }else {
            console.log('Error:' + err);
        }
    });
};

neoService.prototype.getBestMonth = function(req, res, next) {
    var hazardous = req.params.hazardous ? req.params.hazardous : false;

    NEO.find({isHazardous : hazardous}, function(err, result) {
        if (!err){
            var bestMonth = self.extractDatePart(result, 1); //index 1 corresponds to month on date string
            next(bestMonth);
        }else {
            console.log('Error:' + err);
        }
    });
};

neoService.prototype.save = function(newNeo, callback) {
    newNeo.save(function(err) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, 'neo saved successfully.');
        }
    });
};

neoService.prototype.getUrl = function(start, end) {
    var baseUrl = 'https://api.nasa.gov/neo/rest/v1/feed';
    var startDate = 'start_date=' + start;
    var endDate = 'end_date=' + end;
    var apiKey = 'api_key=N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD';

    url = baseUrl + '?' + startDate + '&' + endDate + '&' + apiKey;
    return url;
};

neoService.prototype.printError = function(err, status) {
    if(err) {
        console.log('Error:' + err);
    } else {
        //console.log(status);
    }
};

neoService.prototype.extractDatePart = function(result, part) {
    var dctDates = {};

    for (var neoObj in result) {
        var datePart = (result[neoObj]._doc.date + "").split(" ")[part];

        if (dctDates.hasOwnProperty(datePart)) {
            dctDates[datePart] += 1;
        } else {
            dctDates[datePart] = 1;
        }
    }
    var count = 0;
    var best = '';

    for (var obj in dctDates) {
        if (dctDates[obj] > count) {
            count = dctDates[obj];
            best = obj;
        }
    }
    return best;
};

module.exports = neoService;