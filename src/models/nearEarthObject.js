var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

var nearEarthObjectSchema = new Schema({
    date: Date,
    reference: { type: String, required: true},
    name: { type: String, required: true },
    speed: Number,
    isHazardous: Boolean
});

var NearEarthObject = mongoose.model('NearEarthObject', nearEarthObjectSchema);

module.exports = NearEarthObject;
