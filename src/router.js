var express = require('express');
var neoController = require('./controllers/neoController');
var router = express.Router();

router.get('/', function(req, res) {
	res.json({"Hello":"world!"});
});

router.get('/neo/hazardous', neoController.getHazardous);

router.get('/neo/fastest/:hazardous', neoController.getFastest);

router.get('/neo/best-year/:hazardous', neoController.getBestYear);

router.get('/neo/best-month/:hazardous', neoController.getBestMonth);

module.exports = router;
