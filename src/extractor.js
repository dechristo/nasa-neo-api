var mongoose = require('mongoose');
var NeoService = require('./services/neoService');
var neoService = new NeoService();
var program = require('commander');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/neo', {
    useMongoClient: true
});

program
    .version('1.0.0')
    .description('Near Earth Objects extractor from Nasa API.')
    .usage('[task] <params ...>')
    .option('-e, --extract <start_date> <end_date>', 'Extracts data from NASA API between the given time spam.')
    .option('-e3, --extractLast3Days', 'Extracts data from NASA API from the last 3 days.');

program
    .command('extract <startDate> <endDate>')
    .alias('e')
    .description('Gets data from nasa API.')
    .action(function(startDate, endDate) {
        neoService.extract(startDate, endDate, function(res) {
            console.log('Data successfully extracted from NASA API.');
        });
    });

program
    .command('extractLast3Days')
    .alias('e3')
    .description('Gets data from the last 3 days from nasa API.')
    .action(function() {
        neoService.extractLast3Days(function(res) {});
    });

program.parse(process.argv);