var express = require('express');
var mongoose = require('mongoose');
var router = require('./src/router');
var server = express();

server.use('/api', router);

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/neo', {
    useMongoClient: true
});

server.listen(2000, function() {
	console.log('Server listening on port 2000...');
});

module.exports = server;