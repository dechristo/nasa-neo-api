# Near Earth Objects (NEO) - API

## 1. Frameworks
I have chosen Express.js for creating this app due to its simplicity, minimalism and flexibility.
For testing, the pair mocha and chai.
The app is containerized using Docker and Docker Compose.
Also there's a tool, the **extractor**, responsible for fetching the data from NASA's api and store it in MongoDB.
## 2. Instructions
### 2.1 Run the extractor
From the project root typing `node src/extractor.js --help` gives all the options to run the tool:

	Near Earth Objects extractor from Nasa API.

  	Options:

    -V, --version                          output the version number
    -e, --extract <start_date> <end_date>  Extracts data from NASA API between the given time spam.
    -e3, --extractLast3Days                Extracts data from NASA API from the last 3 days.
    -h, --help                             output usage information

	  Commands:

    extract|e <startDate> <endDate>  Gets data from nasa API.
    extractLast3Days|e3              Gets data from the last 3 days from nasa API.


### 2.2 Run the app
You can run the app by typing `node server.js` at the project's root folder or by building/running the container as describe in item 4.
After the server is started you can test the app accessing its endpoints (described in item 3) using the browser or curl for example. For instance `http://localhost:2000/api` should return: 

	{
		"Hello":"world!"
	}

Also there's already a database dump on `neos.json` file that can be imported with the command: 

	mongoimport --db neo --collection nearearthobjects --file neos.json
	
### 2.2 Tests
To run the tests type 'npm test'. Mocha must be installed (to install it globally type `npm install -g mocha`.
The output should looks like:

  	NearEarthObject Model
    ✓ should be invalid if name and reference are non existent.
    ✓ should be invalid if reference is empty.
    ✓ should be invalid if name is empty.
    ✓ should be valid if name and reference are not empty.

  	Router endpoints
    GET /api
      ✓ should returns HTTP 200 and Hello World json.
    GET api/neo/hazardous
      ✓ should returns HTTP 200 and json with hazardous neos.
    GET api/neo/fastest/:hazardous
      ✓ should returns HTTP 200 and json with the fastest hazardous neo.
      ✓ should returns HTTP 200 and json with the fastest non hazardous neo.
    GET api/neo/best-year/:hazardous
      ✓ should returns HTTP 200 and json with the year with most hazardous neos.
      ✓ should returns HTTP 200 and json with the year with most non hazardous neos.
    GET api/neo/best-month/:hazardous
      ✓ should returns HTTP 200 and json with the month with most hazardous neos.
      ✓ should returns HTTP 200 and json with the month with most non hazardous neos.

 	 NearEarthObject Service
    ✓ should have neos array defined.
    ✓ should have extract method defined.
    ✓ should have save method defined.
    ✓ should have getUrl method defined.
    getUrl(startDate, endDate)
      ✓ should return correct url for Nasa NEO Feed API.
    save()
      ✓ should save object with correct values
    extractLast3Days()
      ✓ should return an Near Earth Object object for valid url on the last 3 days.
    extract()
      ✓ should return an Near Earth Object object for valid: url, start and end dates.
    getHazardous()
      ✓ should return only harzardous neos.
    getFastest(hazardous=true)
      ✓ should return the fastest harzardous neo.
    getFastest(hazardous=false)
      ✓ should return the fastest non harzardous neo.
    getBestYear(hazardous=true)
      ✓ should return the year with most harzardous neo.
    getBestYear(hazardous=false)
      ✓ should return the year with most non harzardous neo.
    getBestMonth(hazardous=true)
      ✓ should return the month with most hazardous neos.
    getBestMonth(hazardous=false)
      ✓ should return the month with most non hazardous neos.

 	 Date Utils
    subtractDays(date, quantity)
      ✓ should return correct date for up to 7 days ago with yyyy-mm-dd format
    getFullMonthName(abbreviation)
      ✓ should return full month name based on abbreviation
    getTodayOnDefaultFormat()
      ✓  should return today date with format yyyy-mm-dd


  	30 passing (202ms)

## 3. Endpoints
The base url is  `http://localhost:2000`
#### 3.1 GET /api
Returns a json with **Hello world!** to simply test  if the app is working. 
#### 3.2 GET api/neo/hazardous
Returns a json containing an array of hazardous neos.
#### 3.3 GET api/neo/fastest/:hazardous
Returns a json containing the fastest neo with the  hazardous param.
#### 3.4 GET api/neo/best-year/:hazardous
Returns a json containing the year of most ocurrences of neos with the  hazardous param.
#### 3.4 GET api/neo/best-month/:hazardous
Returns a json containing the month of most ocurrences of neos with the  hazardous param.
## 4. Docker
### 4.1 Build the container
`docker build -t <your username>/node-web-app .`
### 4.2 Run the container
`docker run -p 49160:2000 -d <your username>/node-web-app`