/*jshint expr: true*/

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../../server');
var nock = require('nock');
var sinon = require('sinon');

var expect = chai.expect;
chai.use(chaiHttp);

describe('Router endpoints', function() {

    describe('GET /api', function() {

        it('should returns HTTP 200 and Hello World json.', function(done) {
            chai.request(server)
                .get('/api')
                .end(function(err, res){
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    expect(res.body).to.be.deep.equal({Hello:"world!"});
                    done();
                });
        });
    });

    describe('GET api/neo/hazardous', function() {
        it('should returns HTTP 200 and json with hazardous neos.', function(done) {
            chai.request(server)
                .get('/api/neo/hazardous')
                .end(function (err, res) {
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    done();
                });
        });
    });

    describe('GET api/neo/fastest/:hazardous', function() {
        it('should returns HTTP 200 and json with the fastest hazardous neo.', function(done) {
            chai.request(server)
                .get('/api/neo/fastest/true')
                .end(function (err, res) {
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    done();
                });
        });

        it('should returns HTTP 200 and json with the fastest non hazardous neo.', function(done) {
            chai.request(server)
                .get('/api/neo/fastest/false')
                .end(function (err, res) {
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    done();
                });
        });
    });

    describe('GET api/neo/best-year/:hazardous', function() {
        it('should returns HTTP 200 and json with the year with most hazardous neos.', function(done) {
            chai.request(server)
                .get('/api/neo/best-year/true')
                .end(function (err, res) {
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    done();
                });
        });

        it('should returns HTTP 200 and json with the year with most non hazardous neos.', function(done) {
            chai.request(server)
                .get('/api/neo/best-year/false')
                .end(function (err, res) {
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    done();
                });
        });
    });

    describe('GET api/neo/best-month/:hazardous', function() {
        it('should returns HTTP 200 and json with the month with most hazardous neos.', function(done) {
            chai.request(server)
                .get('/api/neo/best-month/true')
                .end(function (err, res) {
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    done();
                });
        });

        it('should returns HTTP 200 and json with the month with most non hazardous neos.', function(done) {
            chai.request(server)
                .get('/api/neo/best-month/false')
                .end(function (err, res) {
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    done();
                });
        });
    });
});