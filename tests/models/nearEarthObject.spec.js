/*jshint expr: true*/

var Neo = require('../../src/models/nearEarthObject');
var chai = require('chai');
var sinon = require('sinon');
var expect = chai.expect;

describe('NearEarthObject Model', function () {
	
	it('should be invalid if name and reference are non existent.', function(done) {
		var neo = new Neo({});
	
		neo.validate(function(err) {
			expect(err.errors.reference).to.exist;
			expect(err.errors.name).to.exist;
            expect(err.errors.date).to.not.exist;
            expect(err.errors.speed).to.not.exist;
            expect(err.errors.isHazardous).to.not.exist;
			done();
		});
	});

	it('should be invalid if reference is empty.', function(done) {
		var neo = new Neo({
			name : 'name_test',
			reference: ''
		});

		neo.validate(function(err) {
			expect(err.errors.reference).to.exist;
            expect(err.errors.name).to.not.exist;
            expect(err.errors.date).to.not.exist;
            expect(err.errors.speed).to.not.exist;
            expect(err.errors.isHazardous).to.not.exist;
			done();
		});
	});

	it('should be invalid if name is empty.', function(done) {
        var neo = new Neo({
            name : '',
            reference: 'WQ4577X'
        });

        neo.validate(function(err) {
            expect(err.errors.name).to.exist;
            expect(err.errors.reference).to.not.exist;
            expect(err.errors.date).to.not.exist;
            expect(err.errors.speed).to.not.exist;
            expect(err.errors.isHazardous).to.not.exist;
            done();
        });
	});

    it('should be valid if name and reference are not empty.', function(done) {
        var neo = new Neo({
            name : 'One huge asteroid',
            reference: 'WQ4577X'
        });

        neo.validate(function(err) {
            expect(err).to.be.null;
            done();
        });
    });
});
