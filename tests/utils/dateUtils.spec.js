/*jshint expr: true*/

var dateFormat = require('dateformat');
var dateUtils = require('../../src/utils/dateUtils');
var chai = require('chai');
var expect = chai.expect;

var dtUtils;

describe('Date Utils', function () {

    beforeEach(function () {
        dtUtils = new dateUtils();
    });

    describe('subtractDays(date, quantity)', function() {

        it('should return correct date for up to 7 days ago with yyyy-mm-dd format', function () {
            for (var index = 1; index <= 7; index++) {
                var daysAgo = new Date().setDate((new Date()).getDate() - index);
                var formattedDaysAgo = dateFormat(daysAgo, self.defaultFormat);
                expect(dtUtils.subtractDays(null, index)).to.be.equal(formattedDaysAgo);
            }
        });
    });

    describe('getFullMonthName(abbreviation)', function() {

        it('should return full month name based on abbreviation', function () {
            expect(dtUtils.getFullMonthName('Jan')).to.be.equal('January');
            expect(dtUtils.getFullMonthName('Feb')).to.be.equal('February');
            expect(dtUtils.getFullMonthName('Mar')).to.be.equal('March');
            expect(dtUtils.getFullMonthName('Apr')).to.be.equal('April');
            expect(dtUtils.getFullMonthName('May')).to.be.equal('May');
            expect(dtUtils.getFullMonthName('Jun')).to.be.equal('June');
            expect(dtUtils.getFullMonthName('Jul')).to.be.equal('July');
            expect(dtUtils.getFullMonthName('Aug')).to.be.equal('August');
            expect(dtUtils.getFullMonthName('Sep')).to.be.equal('September');
            expect(dtUtils.getFullMonthName('Oct')).to.be.equal('October');
            expect(dtUtils.getFullMonthName('Nov')).to.be.equal('November');
            expect(dtUtils.getFullMonthName('Dec')).to.be.equal('December');
        });
    });

    describe('getTodayOnDefaultFormat()', function() {

        it(' should return today date with format yyyy-mm-dd', function() {
            var today = new Date();
            var formattedToday = dateFormat(today, self.defaultFormat);
            expect(dtUtils.getTodayOnDefaultFormat()).to.be.equal(formattedToday);
        });
    });
});