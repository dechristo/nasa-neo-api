/*jshint expr: true*/

var NeoService = require('../../src/services/neoService');
var NearEarthObject = require('../../src/models/nearEarthObject');
var chai = require('chai');
var nock = require('nock');
var sinon = require('sinon');
require('sinon-mongoose');
var expect = chai.expect;
var NearEarthObjectMock = sinon.mock(NearEarthObject);

var neoService;
var neo;
var start;
var end;
var apiKey = 'N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD';

describe('NearEarthObject Service', function () {

    beforeEach(function() {
       neoService = new NeoService();
       neo = new NearEarthObject();
    });

    it('should have neos array defined.', function () {
        expect(neoService.neos).to.be.an('array');
    });

    it('should have extract method defined.', function () {
        expect(neoService.extract).to.be.a('function');
    });

    it('should have save method defined.', function () {
        expect(neoService.save).to.be.a('function');
    });

    it('should have getUrl method defined.', function () {
        expect(neoService.getUrl).to.be.a('function');
    });

    describe('getUrl(startDate, endDate)', function() {

        it('should return correct url for Nasa NEO Feed API.', function() {
            expect(neoService.getUrl(start, end)).to.be.equal(
                'https://api.nasa.gov/neo/rest/v1/feed?start_date=' + start + '&end_date=' + end + '&api_key=' + apiKey
            );
        });
    });

    describe('save()', function () {

        it('should save object with correct values', function(done) {

            var mock = new NearEarthObject({
                date: '2017-12-03',
                reference: '3XPL77Z',
                name: 'A huge dangerous asteroid',
                speed: 23243.455,
                isHazardous: false
            });
            
            var stub = sinon.mock(mock)
                .expects('save')
                .yields(null, 'neo saved successfully!');

            neoService.save(mock, function (err, status) {
                expect(err).to.be.a('null');
                expect(status).to.be.equal('neo saved successfully.');
                sinon.assert.calledOnce(stub);
                done();
             });
        });
    });

    describe('extractLast3Days()', function() {

        before(function () {
            start = '2017-12-03';
            end = '2017-12-01';
            var mockReponse = {
                "element_count": 3,
                "near_earth_objects": {
                    "2017-12-03": [{
                        "neo_reference_id": "3726710",
                        "name": "(2017 RC)",
                        "is_potentially_hazardous_asteroid": false,
                        "close_approach_data": [{
                            "relative_velocity": {
                                "kilometers_per_second": "19.4850295284",
                                "kilometers_per_hour": "70146.106302123"
                            },
                            "miss_distance": {
                                "kilometers": "4027630.25",
                                "miles": "2502653.5"
                            }
                        }]
                    }],
                    "2017-12-02": [{
                        "neo_reference_id": "7770213",
                        "name": "(2017 RxD)",
                        "is_potentially_hazardous_asteroid": true,
                        "close_approach_data": [{
                            "relative_velocity": {
                                "kilometers_per_second": "19.4850295284",
                                "kilometers_per_hour": "270141216.106302123"
                            },
                            "miss_distance": {
                                "kilometers": "20.25",
                                "miles": "52653.5"
                            }
                        }]
                    }],
                    "2017-12-01": [{
                        "neo_reference_id": "981112",
                        "name": "(2017 PD)",
                        "is_potentially_hazardous_asteroid": true,
                        "close_approach_data": [{
                            "relative_velocity": {
                                "kilometers_per_second": "109.4850295284",
                                "kilometers_per_hour": "4446.106302123"
                            },
                            "miss_distance": {
                                "kilometers": "630.25",
                                "miles": "26530.5"
                            }
                        }]
                    }]
                }
            };

            nock('https://api.nasa.gov')
                .get('/neo/rest/v1/feed?start_date=' + start + '&end_date=' + end + '&api_key=' + apiKey)
                .reply(200, mockReponse);
        });

        it('should return an Near Earth Object object for valid url on the last 3 days.', function(done) {
            neoService.extract(start, end, function(res) {
                expect(res).to.be.an('object');
                expect(res).to.have.property('2017-12-03');
                expect(res['2017-12-03']).to.be.an('array');
                expect(res['2017-12-03'][0]).that.is.not.empty;
                expect(res['2017-12-03'][0]).to.have.property('is_potentially_hazardous_asteroid');
                expect(res['2017-12-03'][0].is_potentially_hazardous_asteroid).to.be.false;

                expect(res).to.have.property('2017-12-02');
                expect(res['2017-12-02']).to.be.an('array');
                expect(res['2017-12-02'][0]).that.is.not.empty;
                expect(res['2017-12-02'][0]).to.have.property('is_potentially_hazardous_asteroid');
                expect(res['2017-12-02'][0].is_potentially_hazardous_asteroid).to.be.true;

                expect(res).to.have.property('2017-12-01');
                expect(res['2017-12-01']).to.be.an('array');
                expect(res['2017-12-01'][0]).that.is.not.empty;
                expect(res['2017-12-01'][0]).to.have.property('is_potentially_hazardous_asteroid');
                expect(res['2017-12-01'][0].is_potentially_hazardous_asteroid).to.be.true;
                done();
            });
        });
    });

    describe('extract()', function() {

        before(function () {
            start = '2017-12-03';
            end = '2017-12-01';
            var mockReponse = {
                "element_count": 20,
                "near_earth_objects": {
                    "2015-09-08": [{
                        "neo_reference_id": "3726710",
                        "name": "(2015 RC)",
                        "estimated_diameter": {
                            "kilometers": {
                                "estimated_diameter_min": 0.0366906138,
                                "estimated_diameter_max": 0.0820427065
                            }
                        },
                        "is_potentially_hazardous_asteroid": false,
                        "close_approach_data": [{
                            "close_approach_date": "2015-09-08",
                            "epoch_date_close_approach": 1441695600000,
                            "relative_velocity": {
                                "kilometers_per_second": "19.4850295284",
                                "kilometers_per_hour": "70146.106302123"
                            },
                            "miss_distance": {
                                "astronomical": "0.0269230459",
                                "lunar": "10.4730644226",
                                "kilometers": "4027630.25",
                                "miles": "2502653.5"
                            },
                            "orbiting_body": "Earth"
                        }]
                    }]
                }
            };

            nock('https://api.nasa.gov')
                .get('/neo/rest/v1/feed?start_date=' + start + '&end_date=' + end + '&api_key=' + apiKey)
                .reply(200, mockReponse);
        });

        it('should return an Near Earth Object object for valid: url, start and end dates.', function(done) {
            neoService.extract(start, end, function(res) {
                expect(res).to.be.an('object');
                expect(res).to.have.property('2015-09-08');
                expect(res['2015-09-08']).to.be.an('array');
                expect(res['2015-09-08'][0]).that.is.not.empty;
                expect(res['2015-09-08'][0]).to.have.property('neo_reference_id');
                expect(res['2015-09-08'][0].neo_reference_id).to.be.equal('3726710');
                expect(res['2015-09-08'][0]).to.have.property('name');
                expect(res['2015-09-08'][0].name).to.be.equal('(2015 RC)');
                expect(res['2015-09-08'][0]).to.have.property('is_potentially_hazardous_asteroid');
                expect(res['2015-09-08'][0].is_potentially_hazardous_asteroid).to.be.false;
                expect(res['2015-09-08'][0]).to.have.property('close_approach_data');
                expect(res['2015-09-08'][0].close_approach_data).to.be.an('Array');
                expect(res['2015-09-08'][0].close_approach_data).that.is.not.empty;
                expect(res['2015-09-08'][0].close_approach_data[0]).to.have.property('relative_velocity');
                expect(res['2015-09-08'][0].close_approach_data[0].relative_velocity).to.have.property('kilometers_per_hour');
                expect(res['2015-09-08'][0].close_approach_data[0].relative_velocity.kilometers_per_hour).to.be.equal('70146.106302123');
                done();
            });
        });
    });

    describe('getHazardous()', function() {
        it('should return only harzardous neos.', function(done) {
            var mockReponse = {
                "element_count": 2,
                "near_earth_objects": {
                    "2017-12-02": [{
                        "neo_reference_id": "7770213",
                        "name": "(2017 RxD)",
                        "is_potentially_hazardous_asteroid": true
                    }, {
                        "neo_reference_id": "XS1",
                        "name": "(2017 Xs1)",
                        "is_potentially_hazardous_asteroid": true
                    }, {
                        "neo_reference_id": "TGV78",
                        "name": "(2017 782A)",
                        "is_potentially_hazardous_asteroid": true
                    }],
                    "2017-12-01": [{
                        "neo_reference_id": "POI2",
                        "name": "(2017 02P2)",
                        "is_potentially_hazardous_asteroid": true
                    }, {
                        "neo_reference_id": "126212",
                        "name": "(2017 882)",
                        "is_potentially_hazardous_asteroid": true
                    }]
                }
            };

            var stub = NearEarthObjectMock
                .expects('find')
                .yields(null, mockReponse);

            neoService.getHazardous(null, null, function (res) {
                expect(res).to.be.a('object');
                expect(res).to.be.have.property('element_count');
                expect(res.element_count).to.be.equal(2);
                expect(res).to.be.have.property('near_earth_objects');
                expect(res).to.not.be.null;

                var neos = res.near_earth_objects;
                for(var obj in neos) {
                    for(neo in neos.obj) {
                        expect( neos.obj.neo.is_potentially_hazardous_asteroid).to.be.true;
                    }
                }
                sinon.assert.calledOnce(stub);
                done();
            });
        });
    });

    describe('getFastest(hazardous=true)', function() {

        it('should return the fastest harzardous neo.', function(done) {

            var mockReponse = [
               {
                "_doc" : {
                    "neo_reference_id": "XS1",
                    "name": "(2017 Xs1)",
                    "is_potentially_hazardous_asteroid": true,
                    "speed" : 99999999.9992
                }},
                { "_doc" : {
                    "neo_reference_id": "POI2",
                    "name": "(2017 02P2)",
                    "is_potentially_hazardous_asteroid": true,
                    "speed": 7777.9865
                }}, {
                "_doc" : {
                    "neo_reference_id": "126212",
                    "name": "(2017 882)",
                    "is_potentially_hazardous_asteroid": true,
                    "speed" : 190.76
                }}
            ];

            var stub = NearEarthObjectMock
                .expects('find')
                .withArgs({ isHazardous: true })
                .yields(null, mockReponse);

            var reqMock = {
                params : {
                    hazardous : true
                }
            };

            neoService.getFastest(reqMock, null, function (res) {
                expect(res).to.be.equals(99999999.9992);
                sinon.assert.calledOnce(stub);
                done();
            });
        });
    });

    describe('getFastest(hazardous=false)', function() {

        it('should return the fastest non harzardous neo.', function(done) {

            var mockReponse = [
                {
                    "_doc" : {
                        "neo_reference_id": "7770213",
                        "name": "(2017 RxD)",
                        "is_potentially_hazardous_asteroid": false,
                        "speed": 2300
                    }},
                    {"_doc" : {
                        "neo_reference_id": "TGV78",
                        "name": "(2017 782A)",
                        "is_potentially_hazardous_asteroid": false,
                        "speed" : 300
                    }}
            ];

            var stub = NearEarthObjectMock
                .expects('find')
                .withArgs({ isHazardous: false })
                .yields(null, mockReponse);

            var reqMock = {
                params : {
                    hazardous : false
                }
            };

            neoService.getFastest(reqMock, null, function (res) {
                expect(res).to.be.equals(2300);
                sinon.assert.calledOnce(stub);
                done();
            });
        });
    });

    describe('getBestYear(hazardous=true)', function() {

        it('should return the year with most harzardous neo.', function(done) {

            var mockReponse = [
            {
                "_doc" : {
                    "date" : "02 Dec T 2017 11 01",
                    "neo_reference_id": "XS1",
                    "name": "(2017 Xs1)",
                    "is_potentially_hazardous_asteroid": true,
                    "speed" : 99999999.9992
                }}, {
                "_doc" : {
                    "date" : "02 Oct T 2012 11 01",
                    "neo_reference_id": "POI2",
                    "name": "(2012 02P2)",
                    "is_potentially_hazardous_asteroid": true,
                    "speed": 7777.9865
                }}, {
                "_doc" : {
                    "date" : "13 Jan T 2012 11 01",
                    "neo_reference_id": "126212",
                    "name": "(2012 882)",
                    "is_potentially_hazardous_asteroid": true,
                    "speed" : 190.76
                    }}
            ];

            var stub = NearEarthObjectMock
                .expects('find')
                .withArgs({ isHazardous: true })
                .yields(null, mockReponse);

            var reqMock = {
                params : {
                    hazardous : true
                }
            };

            neoService.getBestYear(reqMock, null, function (res) {
                expect(res).to.be.equals("2012");
                sinon.assert.calledOnce(stub);
                done();
            });
        });
    });

    describe('getBestYear(hazardous=false)', function() {

        it('should return the year with most non harzardous neo.', function(done) {

            var mockReponse = [
                {
                    "_doc" : {
                        "date" : "22 May T 2017 11 01",
                        "neo_reference_id": "22XS1",
                        "name": "(2017 X221s1)",
                        "is_potentially_hazardous_asteroid": false,
                        "speed" : 55555559.9992
                    }}, {
                    "_doc" : {
                        "date" : "04 May T 2017 11 01",
                        "neo_reference_id": "ASqPOI2",
                        "name": "(2017 Asq02P2)",
                        "is_potentially_hazardous_asteroid": false,
                        "speed": 10000009.677161
                    }}, {
                    "_doc" : {
                        "date" : "13 Feb T 2015 11 01",
                        "neo_reference_id": "trDcDh11",
                        "name": "(2015 trd02)",
                        "is_potentially_hazardous_asteroid": false,
                        "speed" : 2300012190.711216
                    }}
            ];

            var stub = NearEarthObjectMock
                .expects('find')
                .withArgs({ isHazardous: true })
                .yields(null, mockReponse);

            var reqMock = {
                params : {
                    hazardous : true
                }
            };

            neoService.getBestYear(reqMock, null, function (res) {
                expect(res).to.be.equals("2017");
                sinon.assert.calledOnce(stub);
                done();
            });
        });
    });

    describe('getBestMonth(hazardous=true)', function() {
        it('should return the month with most hazardous neos.', function(done) {

            var mockReponse = [
                {
                    "_doc" : {
                        "date" : "02 May N T 2017 11 01",
                        "neo_reference_id": "XS1",
                        "name": "(2017 Xs1)",
                        "is_potentially_hazardous_asteroid": true,
                        "speed" : 99999999.9992
                    }}, {
                    "_doc" : {
                        "date" : "02 May T 2012 11 01",
                        "neo_reference_id": "POI2",
                        "name": "(2012 02P2)",
                        "is_potentially_hazardous_asteroid": true,
                        "speed": 7777.9865
                    }}, {
                    "_doc" : {
                        "date" : "13 Dec T 2012 05 01",
                        "neo_reference_id": "126212",
                        "name": "(2012 882)",
                        "is_potentially_hazardous_asteroid": true,
                        "speed" : 190.76
                    }}
            ];

            var stub = NearEarthObjectMock
                .expects('find')
                .withArgs({ isHazardous: true })
                .yields(null, mockReponse);

            var reqMock = {
                params : {
                    hazardous : true
                }
            };

            neoService.getBestMonth(reqMock, null, function (res) {
                expect(res).to.be.equals("May");
                sinon.assert.calledOnce(stub);
                done();
            });
        });
    });

    describe('getBestMonth(hazardous=false)', function() {
        it('should return the month with most non hazardous neos.', function(done) {

            var mockReponse = [
                {
                    "_doc" : {
                        "date" : "02 Feb N T 2017 11 01",
                        "neo_reference_id": "XS1",
                        "name": "(2017 Xs1)",
                        "is_potentially_hazardous_asteroid": false,
                        "speed" : 99999999.9992
                    }}, {
                    "_doc" : {
                        "date" : "02 May T 2012 11 01",
                        "neo_reference_id": "POI2",
                        "name": "(2012 02P2)",
                        "is_potentially_hazardous_asteroid": false,
                        "speed": 7777.9865
                    }}, {
                    "_doc" : {
                        "date" : "13 Feb T 2012 05 01",
                        "neo_reference_id": "126212",
                        "name": "(2012 882)",
                        "is_potentially_hazardous_asteroid": false,
                        "speed" : 190.76
                    }}, {
                    "_doc" : {
                        "date" : "13 Feb T 2012 05 01",
                        "neo_reference_id": "126212",
                        "name": "(2012 882)",
                        "is_potentially_hazardous_asteroid": false,
                        "speed" : 190.76
                    }}, {
                    "_doc" : {
                        "date" : "13 Aug T 2012 05 01",
                        "neo_reference_id": "126212",
                        "name": "(2012 882)",
                        "is_potentially_hazardous_asteroid": false,
                        "speed" : 190.76
                    }}
            ];

            var stub = NearEarthObjectMock
                .expects('find')
                .withArgs({ isHazardous: true })
                .yields(null, mockReponse);

            var reqMock = {
                params : {
                    hazardous : true
                }
            };

            neoService.getBestMonth(reqMock, null, function (res) {
                expect(res).to.be.equals("Feb");
                sinon.assert.calledOnce(stub);
                done();
            });
        });
    });
});
